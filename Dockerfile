FROM registry.gitlab.com/api-intergration/server-settings/base:latest as DNAC-Automation

ARG SYSTEM
ARG DNACID
ARG SHARED
ARG DNACUTILS
ARG TEAMSUTILS
ARG TASKSETUP
ARG TOKEN
ENV TOKEN=$TOKEN
ENV DNACID=$DNACID
ENV SHARED=$SHARED
ENV TEAMSUTILS=$TEAMSUTILS
ENV SYSTEM=$SYSTEM
ENV DNACUTILS=$DNACUTILS
ENV TASKSETUP=$TASKSETUP

#RUN pip install git+https://__token__:$TOKEN@gitlab.com/api-intergration/utils/shared.git
RUN pip install git+https://__token__:$TOKEN@gitlab.com/api-intergration/utils/dnac-utils.git
RUN pip install --upgrade  dnacentersdk
RUN echo $DNAC_ID
WORKDIR /opt/app
RUN curl --header "PRIVATE-TOKEN: $TOKEN"  https://gitlab.com/api/v4/projects/$DNACID/repository/archive.tar.gz -O
RUN tar -zxvf /opt/app/archive.tar.gz -C /opt/app --strip-components 1




#USER root
#RUN rm -R /opt/app/

WORKDIR /opt/app
#CMD dnacsync tasks start




