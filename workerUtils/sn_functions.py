#Need to install requests package for python
#easy_install requests
import requests
from os import environ
from .utils_logging import apilogging
# Set the request parameters
def update_switch_record(SN_SYS,tabel):
    url = f"{environ['SERVICE_NOW_URL']}/api/now/table/{tabel}/{SN_SYS}"
    #dev91737.service-now.com
	#cmdb_ci_ip_switch
    # Eg. User name="admin", Password="admin" for this code sample.
    user = environ['SERVICE_NOW_USERNAME']
    pwd = environ['SERVICE_NOW_PASSWORD']

    # Set proper headers
    headers = {"Content-Type":"application/json","Accept":"application/json"}

    # Do the HTTP request
    payload = {"dnacadded":"true"}
    try:
        response = requests.patch(url, auth=(user, pwd), headers=headers ,data=payload)
        # Check for HTTP codes other than 200
        if response.status_code != 200:
            apilogging.debug('Status: ', response.status_code, 'Headers: ', response.headers, 'Error Response: ',response.json())
            return "Service Now record updated"
    except Exception as e:
        apilogging.debug(f"Error updating Service-Now: {str(e)}")
        return (f"Error updating Service-Now: {str(e)}")

