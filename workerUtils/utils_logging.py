import logging
import os

apilogging = logging
if os.environ['RUN_STATE'] == 'DEV':
    for handler in apilogging.root.handlers[:]:
        apilogging.root.removeHandler(handler)
    apilogging.basicConfig(filename='DNAC-server.log',
                           level=apilogging.DEBUG,
                           format='%(asctime)s %(levelname)s %(message)s')
    apilogging.debug('Local Development apilogging Enabled')
else:
    for handler in apilogging.root.handlers[:]:
        apilogging.root.removeHandler(handler)
    apilogging.basicConfig(filename='/opt/logs/DNAC-server.log',level=apilogging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')
    apilogging.debug('Local Production apilogging Enabled')
