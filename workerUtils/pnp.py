import dnacflows.utils
import dnacflows.payloads
import APIBase.teamsutils
from .utils_logging import apilogging
from dnacentersdk import dnacentersdkException
from .sn_functions import update_switch_record
log = apilogging
class PnP(object):
	def create_pnp_and_claim(self ,task ,device ,email):
		"""
		
		Args:
			task(object):
			device(dict):
			email(basestring):

		Returns:

		"""
		apilogging.debug(f'Device: {str(device)}')
		if device.dnac_provisioned:
			APIBase.teamsutils.send_message.all(
				f"Device {device.serial} has been provision in DNA Center all ready")
			task.update_state(status='FAILED')
		else:
	
			dnacsdk = dnacflows.utils.initDNAC()
			dnac = dnacsdk.dnac
			try:
				payload = dnacflows.payloads.pnp()
				sending = payload.new_device_payload(device)
				print(str(sending))
				dnac.pnp.add_device(payload=sending)
				msg = (f"Device serial number: {str(device.serial)} "
					   f"is now added to DNA Center's PnP Database")
				response = True
			except Exception as e:
				log.debug(str(2))
				msg = f"Error Message: {str(e)}"
				APIBase.teamsutils.send_message.replay(msg,email)
				response = False

			if response:
				APIBase.teamsutils.send_message.replay(msg, email)
				siteid = dnacsdk.sites.get_site_id_by_name(str(device.location))
				task.update_state(status='PENDING')
				try:
					pnpid = dnac.pnp.get_device_list(
						serial_number=device.serial)
					pnpid = pnpid[0]["id"]
				except (Exception, dnacentersdkException) as e:
					log.debug(f'Location id error: {str(e)}')
					log.debug(f'Location: {device.location} not found')
					pnpid = False

				task.update_state(status='PENDING')
				if siteid != False and pnpid != False:
					try:
						payloadbuilder = dnacflows.payloads.pnp()
						payload = payloadbuilder.\
							claim_pnp_device_payload(pnpid,siteid)
						dnac.pnp.claim_a_device_to_a_site(payload=payload)
						response = True
					except dnacentersdkException as e:
						response = False

					if response:
						APIBase.teamsutils.send_message.replay(
							f"Device serial number: {str(device.serial)} "
							f"has been provisioned for {str(device.location)}"
							f"\n Waiting for {device.name} to be provisioned",
							email)
						device.dnac_provisioned = True
						device.save()

						msg = update_switch_record(
							str(device.SN_sys_id), "cmdb_ci_ip_switch")
						APIBase.teamsutils.send_message.replay(msg, email)

					else:
						APIBase.teamsutils.send_message.replay(
							f"Error claiming serial number: {str(device.serial)}",
							email)
						task.update_state(status='FAILED')
				else:
					APIBase.teamsutils.send_message.replay(
						"Error please have admin check logs device not claimed",
						email)
					task.update_state(status='FAILED')
			else:
				APIBase.teamsutils.send_message.replay(
					f"Error device serial number: {str(device.serial)} not added to DNAC",
					email)
				task.update_state(status='FAILED')