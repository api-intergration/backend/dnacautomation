"""
to init the dnac SDK call dnac = dnac.utils.initDNAC() the DNAC SDK will take the the enviroment cariabled by default to authincate into DNAC
lower case dnac DNAC API calles using hte DNACSDK https://dnacentersdk.readthedocs.io/en/latest/index.html
Upper Case DNAC.utils makes cutom compund function calls and has the code to seup the dnac varubale
"""
import os
import json
import datetime
import workerUtils
import APIBase.teamsutils
import dnacflows.utils
from tasksetup.setup_celery import start_celery
from APIBase.models.datamodel import device_model

celery = start_celery()
log = workerUtils.logging
@celery.task(bind=True, name='DNAC:add_pnp_and_claim')
def worker_add_pnp_and_claim(self, did ,email):
    """
    ands and claims a new PnP device, the internal device id (did) is passed to the function via the task server
    call a device lookup is performed and the object device is populated based on the model provied
    :param self:
    :param did:
    :param email:
    :return:
    """
    print(f'Device id {did}')
    print(f'Users Email address: {email}')
    device = device_model.find_by_id(did)
    print(f'Device returned form DB: {str(device)}')
    task = self
    print(f'task: {str(task)}')
    pnptask = workerUtils.PnP()
    pnptask.create_pnp_and_claim(task=task,device=device,email=email)

@celery.task(bind=True,name='DNAC:device_health')
def worker_device_health(self,deviceip,email):
    """
    returns device health to a teams BOT
    :param deviceip:
    :param email:
    :return:
    """
    dnacutils= dnacflows.utils.initDNAC()
    msg = dnacutils.health.get_device_health(deviceip)
    msg =  json.dumps(msg, indent=4, sort_keys=True)
    APIBase.teamsutils.send_message.replay(msg, email)

@celery.task(bind=True,name='DNAC:overall_network_health')
def worker_overall_network_health(self,email):
    """
    returns overall network health to a teams bot
    :param email:
    :return:
    """
    dnacutils=dnacflows.utils.initDNAC()
    msg = dnacutils.dnac.networks.get_overall_network_health().response
    card = APIBase.teamsutils.createHealtyCard(msg)
    APIBase.teamsutils.replayCard(card, email, str(msg))

@celery.task(bind=True,name='DNAC:list_all_devices')
def worker_list_all_devices(self,email):
    """
    teturns a list of all devices to a teams bot
    :param email:
    :return:
    """
    workerUtils.logging.debug('Received Info')
    dnacutils= dnacflows.utils.initDNAC()
    workerUtils.logging.debug(str(dnacutils))
    response = dnacutils.dnac.devices.get_device_list().response
    cols=[['Hostname','hostname'],['Serial','serialNumber'],['Ip Address','managementIpAddress']]
    greeting=f"DNA Center Configured Devices"
    card= APIBase.teamsutils.createThreeColWithHeadersCard(response, greeting, cols)
    msg="ERROR ERROR"
    APIBase.teamsutils.replayCard(card, email, msg)

@celery.task(bind=True,name='DNAC:get_device_health')
def worker_get_device_health(self,email,device):
    """
    returns device health to a teamsbot
    :param email:
    :param device:
    :return:
    """
    dnacutils=dnacflows.utils.initDNAC()
    response = dnacutils.health.get_device_health(device)
    cols=[['Site','location'],['Model Number','platformId'],['Serial Number','serialNumber'],
        ['Software Version','softwareVersion'],['Status','communicationState'],
        ['Overall Health Score','overallHealth'],['CPU Health Score','cpuScore'],
        ['Memory Health Score','memoryScore'],
        ['CPU Utilization','cpu'],['Memory Utilization','memory']]
    greeting=f"Status for {str(device)}"
    ts = str(response['timestamp'])
    ts = float(ts)/1000
    timestamp = datetime.datetime.fromtimestamp(float(ts),)\
        .strftime('%m/%d/%y %H:%M:%S')
    card= APIBase.teamsutils.createTwoColCard(response, greeting, cols, timestamp)
    msg = "ERRPR ERROR"
    APIBase.teamsutils.replayCard(card, email, msg)

@celery.task(bind=True,name='DNAC:manual_pnp_device')
def worker_manual_pnp_device(self,email):
    card= APIBase.teamsutils.manualDevice()
    msg = "Error Error"
    APIBase.teamsutils.replayCard(card, email, msg)


@celery.task(bind=True,name='DNAC:add_new_ise_server')
def worker_add_new_ise_server(self,email,details):
    dnacutils = dnacflows.utils.initDNAC()
    dnacutils.apis.ISE.add_new_ise_server(details['ip'],details['username'],
                                          details['password'],details['fqdn'],
                                          details['secret'],)
